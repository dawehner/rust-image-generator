#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate image;
extern crate hyper;
extern crate futures;
extern crate rocket;

use image::{ImageBuffer};
use std::path::{Path};
use rocket::response::NamedFile;
use std::u8;


fn generate_image(width: u32, height: u32) {
	let img = ImageBuffer::from_fn(width, height, |x, y| {
		if (x + y) % 5 == 0 {
			image::Luma([0u8])
		} else {
			image::Luma([255u8])
		}
	});
    // @todo Skip writing the file but stream it directly
	let filename = "foo.png";
	img.save(filename).unwrap();
}

fn generate_colorimage(width: u32, height: u32, r: u8, g: u8, b: u8) {
	let img = ImageBuffer::from_fn(width, height, |x, y| {
        image::Rgb {
          data: [r, g, b]
        }
	});
	let filename = "color.png";
	img.save(filename).unwrap();
}

#[get("/<width>/<height>")]
fn image(width: u32, height: u32) -> Option<NamedFile> {
    generate_image(width, height);
    NamedFile::open(Path::new("foo.png")).ok()
}

#[get("/<width>/<height>/<color>")]
fn image_with_color(width: u32, height: u32, color: String) -> Option<NamedFile> {
    let r = u8::from_str_radix(color.get(0..1).unwrap_or("00"), 8).unwrap_or(0u8);
    let g = u8::from_str_radix(color.get(2..3).unwrap_or("00"), 8).unwrap_or(0u8);
    let b = u8::from_str_radix(color.get(4..5).unwrap_or("00"), 8).unwrap_or(0u8);

    generate_colorimage(width, height, r, g, b);
    NamedFile::open(Path::new("color.png")).ok()
}

fn main() {
    // @todo: Add a useful default homepage.
    rocket::ignite().mount("/", routes![image, image_with_color]).launch();
}

